pandoc ../README.md -o index.html -s --lua-filter=img/md_to_html.lua -c img/style.css --template=img/template.html --metadata title="bf-pd : index"
pandoc boeuf.md -o boeuf.html -s --lua-filter=img/md_to_html.lua -c img/style.css --template=img/template.html --metadata title="bf-pd : the BOEUF framework"
pandoc objects.md -o objects.html -s --lua-filter=img/md_to_html.lua -c img/style.css --template=img/template.html --metadata title="bf-pd : objects"
