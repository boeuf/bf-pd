# bf-pd documentation 


* A description of the BOEUF conceptual framework can be found on [this page](boeuf.md).
* A list of the objects in the bf-pd library can be found [here](objects.md)
