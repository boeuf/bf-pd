<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <title>bf-pd : index</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="img/style.css" />
</head>
<body>
<nav>
	<li><a href="index.html">Home</a></li>
	<li><a href="boeuf.html">BOEUF</a></li>
	<li><a href="objects.html">Objects</a></li>
</nav>
<main>
<h1 id="bf-pd-a-puredata-external-for-building-digital-orchestras">bf-pd : a PureData external for building Digital Orchestras</h1>
<h2 id="introduction-what-is-bf-pd-and-why-do-you-need-it">Introduction: What is bf-pd and why do you need it?</h2>
<p>Digital instruments allow us to do things that were not possible with purely acoustic instruments. For example musicians can share data between instruments in real-time, in effect creating distributed multi-musician super-instruments. Musicians such as The Hub have been doing this since at least the 1970s. Contemporary digital musicians often write their own software instruments to re-create these functionalities.</p>
<p>Bf-pd is a library built in PureData (Pd) which enables communication and cooperation between digital instruments. Bf-pd can be integrated into any instrument built in Pd. It provides a <em>collaboration window</em> from which musicians can view each others’ activity and share control of instrument parameters and other musical data.</p>
<p>The design of bf-pd is based on the BOEUF conceptual framework which consists of a classification of modes of collaboration used in collective music performance, and a set of components which affords them. More details on the BOEUF conceptual framework can be found <a href="boeuf.html">on this page</a>.</p>
<p>For more information on the BOEUF project, please visit <a href="https://bf-collab.net/">https://bf-collab.net/</a>.</p>
<h2 id="installing-bf-pd">Installing bf-pd</h2>
<ul>
<li>Download and install Pure Data (&gt;=0.51) for your operating system from <a href="https://puredata.info/downloads/pure-data">https://puredata.info/downloads/pure-data</a></li>
<li>Install the <strong>mrpeach</strong> library using Deken (PD -&gt; Help -&gt; Find Externals)</li>
<li>Install <strong>bf-pd</strong> using Deken</li>
</ul>
<h2 id="getting-started-building-your-instrument">Getting started: Building your instrument</h2>
<figure>
<img src="img/start.gif" alt="" /><figcaption>doc/img/start.gif</figcaption>
</figure>
<p>Starting from an existing instrument made in PureData :</p>
<ol type="1">
<li>Create a declare object with -path bf-pd</li>
<li>Create a bf-instrument object with your instrument name as argument</li>
<li>Connect the sound output of your instrument to its inlet</li>
<li>Create a bf-param object for each control you want to share with the others and insert it into your instrument</li>
<li>Create a bf-session object with the name of the session you want to connect to</li>
</ol>
<h3 id="parameters-and-outputs">Parameters and outputs</h3>
<p><strong>Parameters</strong> correspond to controls you have over your instrument and that you want to share with others.</p>
<figure>
<img src="img/bf-param.gif" alt="" /><figcaption>doc/img/bf-param.gif</figcaption>
</figure>
<p><strong>Outputs</strong> have the same properties but correspond to musical content produced by your instruments (notes, onsets, envelopes …). They can be used to control others parameters. Three default outputs are automatically created by bf-pd and extracted from the audio you send to bf-instrument : pitch, onsets, loudness.</p>
<figure>
<img src="img/bf-output.gif" alt="" /><figcaption>doc/img/bf-output.gif</figcaption>
</figure>
<p>Parameters and outputs have the same <strong>arguments</strong> :</p>
<ol type="1">
<li>instrument name</li>
<li>parameter / output namenumberOfValues valuesType parameterIndex ]</li>
<li>number of values</li>
<li>type of values : cont (0 to 1), midi (0 to 127), bool (0 or 1), bang</li>
<li>index : position in the collaboration window</li>
</ol>
<h2 id="performance-making-music-together-using-bf-pd">Performance: Making music together using bf-pd</h2>
<h3 id="the-collaboration-window">The collaboration window</h3>
<p>The collaboration window is a collaboration interface that comes with bf-pd and provides a way to connect your instrument with other instruments in the session.</p>
<p>To open the collaboration window, simply check the <em>collab</em> toggle in the bf-instrument object.</p>
<p>The leftmost green column shows your instrument with its outputs and parameters. The grey columns show the other instruments in the session with their respective activity, outputs and parameters.</p>
<figure>
<img src="img/collabwin.png" alt="" /><figcaption>doc/img/collabwin.png</figcaption>
</figure>
<h3 id="sharing-granting-access-to-your-parameters">Sharing / granting access to your parameters</h3>
<p>By default, all your parameters are in the <em>granted</em> mode, which means that all the other musicians can change your parameter values. You can of course change this by clicking the red <em>grant</em> toggle for each parameter, or the <em>grant all</em> toggle at the top of your column. When your parameter is not granted, values <em>asked</em> by others are only displayed on the gray widgets. You can accept their <em>ask</em> by using the <em>1x</em> bang.</p>
<figure>
<img src="img/sharing.gif" alt="" /><figcaption>doc/img/sharing.gif</figcaption>
</figure>
<h3 id="using-others-parameters-and-outputs-to-change-your-parameters">Using others parameters and outputs to change your parameters</h3>
<p>A <em>watch</em> bus selector is placed below each parameter and output of the other instruments. By choosing a bus other than 0, the values are sent to the corresponding bus and can be sent to your parameters connected to the same bus using the <em>watching</em> selector.</p>
<figure>
<img src="img/watching.gif" alt="" /><figcaption>doc/img/watching.gif</figcaption>
</figure>
<h3 id="controlling-others-parameters-with-your-parameters-and-outputs">Controlling others parameters with your parameters and outputs</h3>
<p>Symmetrically, you can control others parameters by choosing a non-zero bus on the <em>ask</em> selector and the same <em>asking</em> bus below one of your outputs or parameters. This requires however that the parameter that you want to controlled is <em>granted</em> (i.e. that the green <em>granted</em> toggle is checked).</p>
<figure>
<img src="img/asking.gif" alt="" /><figcaption>doc/img/asking.gif</figcaption>
</figure>
<h3 id="directly-asking-controlling-others-parameters">Directly asking / controlling others parameters</h3>
<p>You can also ask another instrument’s parameter to be set to a specific value by interacting with the white sliders/toggles/bangs. It the access is granted, the value will be directly set.</p>
<figure>
<img src="img/direct_asking.gif" alt="" /><figcaption>doc/img/direct_asking.gif</figcaption>
</figure>
<h2 id="demo-videos">Demo Videos</h2>
<p> Video demoing the collaboration window in bf-pd</p>
<p><br/> <iframe src="http://pod.univ-lille.fr/video/19614-bf-pd-collaboration-window/?is_iframe=true" width="640px" height="360px" allowfullscreen></iframe> </p>
<p><br/></p>
<p> Video demoing bf-clock in bf-pd</p>
<p><br/> <iframe src="https://pod.univ-lille.fr/video/20945-bf-pd-using-bf-clock/?is_iframe=true" width="640px" height="360px" allowfullscreen></iframe> </p>
<h2 id="more-resources">More resources</h2>
<p>Documentation for each of the user objects in bf-pd can be found in <a href="objects.html">objects.md</a> and on help patches directly in PureData (right-click an object -&gt; Help)</p>
<p>The <em>BOEUF conceptual framework</em> describes the <em>modes of collaboration</em> used in musical collaboration and describes a set of <em>components</em> for realizing these modes in a software framework. Bf-pd is based on this framework. More info can be found in <a href="boeuf.html">boeuf.md</a>.</p>
<p>For more information on the BOEUF project, please visit <a href="https://bf-collab.net/">https://bf-collab.net/</a>.</p>
<h2 id="authors">Authors</h2>
<ul>
<li>Luke Dahl, University of Virginia</li>
<li>Florent Berthaut, University of Lille</li>
</ul>
</main>
</body>
</html>
