function Link(el)
	el.target = string.gsub(el.target, "%.md", ".html")
	el.target = string.gsub(el.target, "doc/", "")
	return el
end

function Image(el)
	el.src = string.gsub(el.src, "doc/", "")
	return el
end

function RawInline (raw)
	if raw.format == 'html' then 
		txt = raw.text
		if string.find(txt, "<a") then 
			txt = ""
		end
		if string.find(txt, "</a>") then
			txt = ""
		end
		if string.find(txt, "img/video.png") then
			txt = "<iframe src=\"http://pod.univ-lille.fr/video/19614-bf-pd-collaboration-window/?is_iframe=true\" width=\"640px\" height=\"360px\" allowfullscreen></iframe>"
		end
		if string.find(txt, "img/video_clock.png") then
			txt = "<iframe src=\"https://pod.univ-lille.fr/video/20945-bf-pd-using-bf-clock/?is_iframe=true\" width=\"640px\" height=\"360px\" allowfullscreen></iframe>"
		end
		txt = string.gsub(txt, "doc/img", "img")
		return pandoc.RawInline('html', txt)
	end
end

