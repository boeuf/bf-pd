Bf-pd is a library of pd objects for enabling real-time collaboration. This page describes each of the objects in bf-pd, their arguments, inlets and outlets. (Note, there is also a help file for each of the bf-pd objects

## bf-instrument 
A bf-instrument represents a musical instrument within the bf-pd system. Each instrument joins a bf-session and can contain bf-params and bf-outputs. To create an instrument you should instantiate a bf-instrument with the name of your instrument, e.g. [bf-instrument myDrumMachine]. 

The audio output of your instrument should be connected to the first inlet of bf-instrument object. The interface has an *activity monitor* which shows a simple representation of the changing spectrum of the instrument's sound, and the activity for each bf-instrument in the bf-session appears in the *collaboration window*. (Note: you must also send your audio to [dac~] in order to hear it!) 
 
The *collaboration window* can be opened with the toggle in the bf-instrument object. If musicians enter or leave the session you may need to close and re-open the collaboration window to update the collab window.

### Arguments:
1. The name of the instrument.

### Inlets:
1. The audio output of your instrument should be sent here. (It must also be sent to [dac~].) 
2. Messages can be sent to set :
	* the session name [session-name mySession(


## bf-param 
A bf-param creates a user parameter within the bf-pd system. A bf-param can be controlled by the user of the bf-instrument it belongs to, and the bf-param will also be accessible to other instruments within the bf-session via the collaboration window (if the owner of the bf-instrument has granted access.)

A bf-param object has a user interface within the instrument patch itself, and it also creates a user interface in the collaboration window of each musician in the session.

A bf-param can be one of four *types*:
* *cont* are numbers between 0.0 and 1.0.
* *midi* are numbers between 0.0 and 127.0.
* *bool* can take values of 0 or 1.
* *bang* are bang messages which are used to trigger actions.

A bf-param can be a single instance of its type or it can be a multiple, which creates an array of values within a single bf-param object. For example, a bf-param "8 bool" creates an interface with 8 toggles, and might be used to represent 8 on/off values in a drum machine sequencer.

### Arguments:
1. The name of the instrument, e.g. "myDrumMachine".
2. The name of the parameter, e.g. "kickOnOff"
3. The dimensionality of the parameter, e.g. "8"
4. The type of the parameter, e.g. "bool"
5. The order you want the parameter to appear in the Collab Window.

So a bf-param with 8 drum machine note triggers would be:
`[bf-param myDrumMachine kickOnOff 8 bool]` and creates this object within your patch:

![bf-param_example](img/bf-param_example.png)

### Inlets:
1. Sets the value of the parameter. If the parameter is a multiple, then *Inlet 1* sets the parameter at the index last received on *Inlet 2*.
2. Sets the index of the parameter which will be changed by sending values to *Inlet 1*.
3. An integer in *Inlet 3* will send the value of the parameter at that index to *Outlet 3*.

(The first two inlets for bf-param operate similar to the first two inlets for [array set] in Pd.)

### Outlets:
1. The parameter value of the most recent change to the bf-param (whether that change came from the inlets, the UI, or via the collaboration window.)
2. A list with the parameter value of the most recent change, and the index of the parameter that was changed.
3. The parameter value for the index that was sent to *Inlet 3*.



## bf-output
An bf-output can be used to send some musical data from your bf-instrument to another instrument in the same bf-session. A bf-output can have the same types and multiplicity as a bf-param.

When you open the collab window (by clicking on the 'collab' toggle in your bf-instrument) 
Each bf-output in your bf-instrument will appear on the left of the column for your instrument. You will also see three default outputs which are computed from your instrument's audio: one detects onsets in your instrument's audio, one outputs a pitch estimate, and one which outputs a loudness estimate. 


### Arguments:
1. The name of your instrument, e.g. "myDrumMachine" 
2. The name of your output, e.g. "triggers"
3. The dimensionality of the parameter, e.g. "4"
4. The type of the parameter, e.g. "bang"
5. The order you want the outlet to appear in the Collab Window (after the three default outputs).

### Inlets:
The inlets are similar to the first two inlets of bf-param
1. Sets the value of the output. If the output is a multiple, then *Inlet 1* sets the parameter at the index last received on *Inlet 2*.
2. Sets the index of the output which will be changed by sending values to *Inlet 1*.

### Outlets:
1. The bf-output object in your patch does not have any outlets.


## bf-clock
A bf-clock can be used for generating and sharing tempi and rhythmic events. You can use a bf-clock to create rhythmic events in your instrument, and you can synchronise instruments by sharing tempi between instruments via the collab window. 

### Arguments:
1. The name of your instrument, e.g. "myDrumMachine" 
2. The name of your bf-clock, e.g. "leadClock"
3. The order you want the clock to appear (amongst the other bf-params) in the Collab Window.

### Inlets:
1. A '1' or '0' turns the clock on and off
2. A number sets the tempo in beats-per-minute (BPM), e.g. "120"
3. A number sets the meter, i.e. the number of beats in a measure, e.g. "4" for 4/4  time.
4. A number sets the first beat subdivision. E.g. "2" will create eighth notes (or quavers).
5. A number sets the second beat subdivision. E.g. "3" will create eighth note triplets (or quaver triplets).
6. This inlet can take a variety of messages for controlling a bf-clock. (See bf-clock-help.pd for details.)

### Outlets:
1. Outputs a bang on each downbeat. E.g. if meter is set to '4', the downbeat will occur every four beats. You can think of this as the first beat in a measure.
2. Outputs a bang on each beat. E.g. if BPM is set to '120', then there will be two beats per second. 
3. Outputs a bang on each subdivision1. E.g. if sub1 is "2", then the bangs can be thought of as eighth notes (quavers).
4. Outputs a bang on each subdivision1. E.g. if sub2 is "3", then the bangs can be thought of as eighth note triplets (quavertriplets).
5. This outlet sends various messages about the state of the bf-clock, which allows one bf-clock to control the bpm of, and synchronize with, another bf-clock within the same patch.

See bf-clock-help.pd to see more details about using bf-clock.
