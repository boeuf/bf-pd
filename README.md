# bf-pd : a PureData external for building Digital Orchestras

## Introduction: What is bf-pd and why do you need it?

Digital instruments allow us to do things that were not possible with purely acoustic instruments. For example musicians can share data between instruments in real-time, in effect creating distributed multi-musician super-instruments. Musicians such as The Hub have been doing this since at least the 1970s. Contemporary digital musicians often write their own software instruments to re-create these functionalities.

Bf-pd is a library built in PureData (Pd) which enables communication and cooperation between digital instruments. Bf-pd can be integrated into any instrument built in Pd. It provides a *collaboration window* from which musicians can view each others’ activity and share control of instrument parameters and other musical data.

The design of bf-pd is based on the BOEUF conceptual framework which consists of a classification of modes of collaboration used in collective music performance, and a set of components which affords them. More details on the BOEUF conceptual framework can be found
[on this page](doc/boeuf.md).

For more information on the BOEUF project, please visit [https://bf-collab.net/](https://bf-collab.net/).

## Installing bf-pd

- Download and install Pure Data (>=0.51) for your operating system from [https://puredata.info/downloads/pure-data](https://puredata.info/downloads/pure-data)
- Install the **mrpeach** library using Deken (PD -> Help -> Find Externals)
- Install **bf-pd** using Deken

## Getting started: Building your instrument

![doc/img/start.gif](doc/img/start.gif)

Starting from an existing instrument made in PureData :

1. Create a declare object with -path bf-pd
2. Create a bf-instrument object with your instrument name as argument
3. Connect the sound output of your instrument to its inlet
4. Create a bf-param object for each control you want to share with the others and insert it into your instrument
5. Create a bf-session object with the name of the session you want to connect to

### Parameters and outputs

**Parameters** correspond to controls you have over your instrument and that you want to share with others.

![doc/img/bf-param.gif](doc/img/bf-param.gif)


**Outputs** have the same properties but correspond to musical content produced by your instruments (notes, onsets, envelopes ...). They can be used to control others parameters.
Three default outputs are automatically created by bf-pd and extracted from the audio you send to bf-instrument : pitch, onsets, loudness.

![doc/img/bf-output.gif](doc/img/bf-output.gif)


Parameters and outputs have the same **arguments** :

1. instrument name
2. parameter / output namenumberOfValues valuesType parameterIndex ]
3. number of values
4. type of values : cont (0 to 1), midi (0 to 127), bool (0 or 1), bang
5. index : position in the collaboration window

## Performance: Making music together using bf-pd

### The collaboration window

The collaboration window is a collaboration interface that comes with bf-pd and provides a way to connect your instrument with other instruments in the session.

To open the collaboration window, simply check the *collab* toggle in the bf-instrument object.

The leftmost green column shows your instrument with its outputs and parameters. The grey columns show the other instruments in the session with their respective activity, outputs and parameters.

![doc/img/collabwin.png](doc/img/collabwin.png)

### Sharing / granting access to your parameters

By default, all your parameters are in the *granted* mode, which means that all the other musicians can change your parameter values. You can of course change this by clicking the red *grant* toggle for each parameter, or the *grant all* toggle at the top of your column.
When your parameter is not granted, values *asked* by others are only displayed on the gray widgets. You can accept their *ask* by using the *1x* bang.

![doc/img/sharing.gif](doc/img/sharing.gif)

### Using others parameters and outputs to change your parameters

A *watch* bus selector is placed below each parameter and output of the other instruments. By choosing a bus other than 0, the values are sent to the corresponding bus and can be sent to your parameters connected to the same bus using the *watching* selector.

![doc/img/watching.gif](doc/img/watching.gif)

### Controlling others parameters with your parameters and outputs

Symmetrically, you can control others parameters by choosing a non-zero bus on the *ask* selector and the same *asking* bus below one of your outputs or parameters. This requires however that the parameter that you want to controlled is *granted* (i.e. that the green *granted* toggle is checked).

![doc/img/asking.gif](doc/img/asking.gif)


### Directly asking / controlling others parameters

You can also ask another instrument's parameter to be set to a specific value by interacting with the white sliders/toggles/bangs. It the access is granted, the value will be directly set.

![doc/img/direct_asking.gif](doc/img/direct_asking.gif)

## Demo Videos

<a href="https://pod.univ-lille.fr/video/19614-bf-pd-collaboration-window/">
Video demoing the collaboration window in bf-pd
	
<br/>
<img src="doc/img/video.png" height="300px"/>
</a>

<br/>

<a href="https://pod.univ-lille.fr/video/20945-bf-pd-using-bf-clock/">
Video demoing bf-clock in bf-pd
	
<br/>
<img src="doc/img/video_clock.png" height="300px"/>
</a>


## More resources

Documentation for each of the user objects in bf-pd can be found in [objects.md](doc/objects.md) and on help patches directly in PureData (right-click an object -> Help)

The *BOEUF conceptual framework* describes the *modes of collaboration* used in musical collaboration and describes a set of *components* for realizing these modes in a software framework. Bf-pd is based on this framework. More info can be found in [boeuf.md](doc/boeuf.md).

For more information on the BOEUF project, please visit [https://bf-collab.net/](https://bf-collab.net/).

## Authors 

* Luke Dahl, University of Virginia
* Florent Berthaut, University of Lille

